const coin = {
    state: 0,
    flip: function() {
        // 1. Um ponto: Randomicamente configura a propriedade “estado” do 
        // seu objeto moeda para ser um dos seguintes valores:
        // 0 ou 1: use "this.state" para acessar a propriedade "state" neste objeto.
        this.state = Math.round(Math.random())
        return this.state;
    },
    toString: function() {
        // 2. Um ponto: Retorna a string "Heads" ou "Tails", dependendo de como
        //  "this.state" está como 0 ou 1.
        switch(this.state) {
            case 0:
                return '>Heads'
            case 1:
                return '>Tails'
        }
    },
    toHTML: function() {
        const image = document.createElement('img')
        image.style.width = '18vw'
        image.style.height = '18vw'
        image.className = 'coins'
        // 3. Um ponto: Configura as propriedades do elemento imagem 
        // para mostrar a face voltada para cima ou para baixo dependendo
        // do valor de this.state está 0 ou 1.
        if (this.state === 0) {
            image.src = './images/Heads.png'
            image.alt = 'coinHead'
        } else {
            image.src = './images/Tails.png'
            image.alt = 'coinTail'
        }
        return image;
    }
 };
 
 function display20Flips() {
    const results = []
    // 4. Um ponto: Use um loop para arremessar a moeda 20 vezes, cada vez 
    // mostrando o resultado como uma string na página. 
    // Depois de que seu loop terminar, retorne um array com o 
    // resultado de cada arremesso.
    const screen = document.createElement('div')
    screen.className = 'screen'
    for (let flips = 0; flips < 20; flips++) {
        results.push(this.flip())
        screen.innerText += ' ' + this.toString()
        document.body.appendChild(screen)
    }
    return results;
 }
 
 function display20Images() {
    const results = []
    // 5. Um ponto: Use um loop para arremessar a moeda 20 vezes, cada vez 
    // mostrando o resultado como uma imagem na página. 
    // Depois de que seu loop terminar, retorne um array com o 
    // resultado de cada arremesso.
    for (let flips = 0; flips < 20; flips++) {
        results.push(this.flip())
        document.body.appendChild(this.toHTML())
    }
    return results 
 }
 display20Flips.call(coin)
 display20Images.call(coin)